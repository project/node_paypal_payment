Node PayPal Payment module for Drupal 8.x
-----------------------------------------
DESCRIPTION
-----------
  Node PayPal Payment module allows admin to accept payment using PayPal when a 
  user creat a node/content. It takes payment using 'PayPal Standard' Method.
  Admin can allow this feature for multiple content types and also able to set 
  amount, currency, thank you page after the payment completion along with 
  thank you and cancel messages etc.

INSTALLATION:
-------------
  1. Extract the tar.gz into your 'modules' or directory and copy to modules
     folder.
  2. Go to "Extend" after successfully login into admin.
  3. Enable the module at 'administer >> modules'.

DEPENDENCIES
------------
  The Node PayPal Payment module has no dependencies, nothing special 
  is required.

CONFIGURATION
-------------
  The configuration page is at admin/content/npp,
  where you can configure the Node PayPal Payment module
  and enable it to allow take payment for node creation.  
  you can set paypal email ID to take payment and other configurations.

UNINSTALLATION
--------------
  1. Disable the module from 'administer >> modules'.
  2. Uninstall the module
